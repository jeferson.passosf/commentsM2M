from mongoengine import *
from datetime import datetime

# APN
# Contador fora da meta
# Porcentagem fora da meta
# Falha
# Comentários

class Comment(Document):

    apn = StringField( required=True )
    counter = StringField( required=True )
    rate = FloatField( required=True )
    failure = StringField( required=True )
    text = StringField( required=True )
    datetime = StringField( default=str(datetime.now()) )
