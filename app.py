from mongoengine import *
from src import *
from flask import Flask, request
from datetime import datetime

app = Flask(__name__)

# by connect default in mongoDB > connect( <db> )
# default > host : localhost, port 27017, no authentication
# no default
# connect(
#     db='test',
#     username='user',
#     password='12345',
#     host='mongodb://localhost/production'
# )

route = "/api/v0.1"
connect('m2m')

responses = {
    "success" : '{ "status" : "success" }',
    "invalid" : '{ "status" : "invalid" }'
}

# creat new documnet Comment
# Method POST
# Content-Type: application/json
# params : {
#   apn : string,
#   counter : string,
#   rate : float,
#   failure : stting,
#   text : string
# }
@app.route(route + '/comment', methods=['POST'])
def newComment():
    print(request.json)
    if not request.json:
        return responses['invalid']

    _comment = request.json
    comment = Comment(**request.json)
    # comment.apn = _comment['apn']
    # comment.counter = _comment['counter']
    # comment.rate = _comment['rate']
    # comment.failure = _comment['failure']
    # comment.text = _comment['text']
    comment.save()

    return responses['success']

# Return all comments
# Method GET
@app.route(route + '/comment', methods=['GET'])
def getComment():
    return Comment.objects().to_json()

# Update comment
# Method PUT
# Content-Type: application/json
# params : {
#   apn : string,
#   counter : string,
#   rate : float,
#   failure : stting,
#   text : string
#   id : string
# }
@app.route(route + '/comment', methods=['PUT'])
def updateComment():
    if not request.json:
        return responses['invalid']

    _id = request.json['id']

    comment = Comment.objects(id=_id)

    _comment = request.json

    comment.apn = _comment['apn']
    comment.counter = _comment['counter']
    comment.rate = _comment['rate']
    comment.failure = _comment['failure']
    comment.text = _comment['text']

    Comment.objects(id=_id).update(**_comment)

    return comment.to_json()

# Delete comment
# Method DELETE
# Content-Type: application/json
# params : {
#   id : string
# }
@app.route(route + '/comment', methods=['DELETE'] )
def deleteComment():
    if not request.json:
        return responses['invalid']

    _id = request.json['id']
    # Comment.objects.delete({ "_id" : _id })

    Comment.objects(id=_id).delete()

    return Comment.objects(apn="test.br").to_json()

# Get comments per APN
# Method GET
@app.route( route + "/comment/apn/<_apn>", methods=['GET'] )
def getCommentApn(_apn):
    return Comment.objects( apn__contains=_apn).to_json()

@app.route( route + "/comment/today/apn/<_apn>" )
def getCommentTodayApn(_apn):
    datenow = datetime.now().strftime("%Y-%m-%d")

    return Comment.objects( apn__contains=_apn, datetime__contains=datenow ).to_json()


if __name__ == '__main__':

	#run(host=None, port=None, debug=None, **options)
	app.run('127.0.0.1', 2334)